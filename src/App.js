import "./App.css";
import { Component } from "react";
import CharacterList from "./components/CharacterList";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      characterList: [],
    };
  }
  componentDidMount() {
    fetch("https://rickandmortyapi.com/api/character/")
      .then((resp) => resp.json())
      .then((resp) => this.setState({ characterList: resp.results }));
  }

  render() {
    return (
      <div className="App-header">
        <CharacterList list={this.state.characterList}></CharacterList>
      </div>
    );
  }
}

export default App;
