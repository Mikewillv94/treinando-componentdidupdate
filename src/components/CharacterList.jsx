import { Component } from "react";
import Character from "./Character";

class CharacterList extends Component {
  render() {
    const array = this.props.list;
    return (
      <>
        {array.map((value, index) => (
          <Character key={index} name={value.name}></Character>
        ))}
      </>
    );
  }
}

export default CharacterList;
